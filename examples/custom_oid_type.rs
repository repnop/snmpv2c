use snmpv2c::{
    oid_safety::{ConstOid, ReadableOid},
    SnmpSession, Value,
};

struct TotalRam(i32);

impl TotalRam {
    const OID: total_ram::TotalRam = total_ram::TotalRam;
}

impl ConstOid for TotalRam {
    fn value() -> &'static [u64] {
        &[1, 3, 6, 1, 4, 1, 143, 101, 4, 5, 0]
    }
}
impl ReadableOid for TotalRam {}

mod total_ram {
    use snmpv2c::oid_safety::{Oid, ReadableOid};

    pub struct TotalRam;
    impl Oid for TotalRam {
        fn value(&self) -> &[u64] {
            &[1, 3, 6, 1, 4, 1, 143, 101, 4, 5, 0]
        }
    }
    impl ReadableOid for TotalRam {}
}

impl std::convert::TryFrom<Value> for TotalRam {
    type Error = &'static str;

    fn try_from(v: Value) -> Result<Self, Self::Error> {
        v.integer().map(TotalRam).ok_or("bad snmp value")
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut session = SnmpSession::new("localhost:0", "localhost:161", "public", None)?;
    let response = session.get(&[TotalRam::OID])?;

    let TotalRam(mut n) = response.extract().unwrap();

    let labels = ["KB", "MB", "GB", "TB", "PB"];
    for label in &labels[..] {
        if n / 1024 == 0 {
            println!("Total available physical RAM: {}{}", n, label);
            break;
        } else {
            n /= 1024
        }
    }

    Ok(())
}
