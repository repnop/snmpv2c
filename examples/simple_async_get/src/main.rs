use snmpv2c::{oid, async_session::SnmpSession, Value};
use async_std::println;

#[async_std::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let total_ram = oid!("1.3.6.1.4.1.2021.4.5.0");
    let mut session = SnmpSession::new("localhost:0", "localhost:161", "public", None).await?;
    let response = session.get(&[&total_ram]).await?;
    let bindings = response.variable_bindings();

    match &bindings[0].value() {
        Value::Integer(n) => {
            let mut n = *n;
            let labels = ["KB", "MB", "GB", "TB", "PB"];
            for label in &labels[..] {
                if n / 1024 == 0 {
                    println!("Total available physical RAM: {}{}", n, label).await;
                    break;
                } else {
                    n /= 1024
                }
            }
        }
        Value::NoSuchObject => println!("No RAM value found :(").await,
        _ => println!("Unexpected value").await,
    }
    
    Ok(())
}
