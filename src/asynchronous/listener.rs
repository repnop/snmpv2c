use crate::{
    req_resp::{Pdus, SnmpPacket},
    session::SnmpResult,
    values::Deserialize,
};
use async_std::net::{ToSocketAddrs, UdpSocket};
use futures::stream::Stream;
use std::net::SocketAddr;

pub struct SnmpListener {
    socket: UdpSocket,
    buffer: Vec<u8>,
}

impl SnmpListener {
    pub async fn bind<A: ToSocketAddrs>(addr: A) -> async_std::io::Result<Self> {
        let socket = UdpSocket::bind(addr).await?;
        let buffer = vec![0; 65535];

        Ok(Self { socket, buffer })
    }

    pub async fn accept(&mut self) -> SnmpResult<(SnmpPacket<Pdus>, SocketAddr)> {
        let (len, sockaddr) = self.socket.recv_from(&mut self.buffer[..]).await?;

        Ok((SnmpPacket::deserialize(&mut &self.buffer[..len])?, sockaddr))
    }

    pub fn incoming(&mut self) -> impl Stream<Item = SnmpResult<SnmpPacket<Pdus>>> + '_ {
        futures::stream::unfold(self, |this| async move {
            let res = this.accept().await.map(|(pkt, _)| pkt);
            Some((res, this))
        })
    }

    pub fn incoming_with_addrs(
        &mut self,
    ) -> impl Stream<Item = SnmpResult<(SnmpPacket<Pdus>, SocketAddr)>> + '_ {
        futures::stream::unfold(self, |this| async move {
            let res = this.accept().await;
            Some((res, this))
        })
    }
}
