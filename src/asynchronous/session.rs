use crate::{
    oid_safety::ReadableOid,
    req_resp::{Response, SnmpPacket, GET_REQUEST_TAG, SET_REQUEST_TAG},
    session::{SnmpError, WritableOidAndValue},
    values::{self, Deserialize, Serialize},
};
use async_std::{
    io::{self, timeout},
    net::{ToSocketAddrs, UdpSocket},
};
use std::time::Duration;
pub struct SnmpSession {
    community_string: String,
    read_buffer: Vec<u8>,
    read_buf_len: usize,
    socket: UdpSocket,
    write_buffer: Vec<u8>,
    request_num: u32,
    timeout: Option<Duration>,
}

impl SnmpSession {
    pub async fn new<A: ToSocketAddrs>(
        peer: A,
        community_string: &str,
        timeout: impl Into<Option<Duration>>,
    ) -> io::Result<Self> {
        let socket = UdpSocket::bind((std::net::Ipv4Addr::new(0, 0, 0, 0), 0)).await?;
        socket.connect(peer).await?;

        Ok(Self {
            community_string: community_string.to_string(),
            socket,
            read_buffer: vec![0; 65535],
            read_buf_len: 0,
            write_buffer: Vec::with_capacity(4096),
            request_num: 0,
            timeout: timeout.into(),
        })
    }

    pub async fn get(&mut self, oids: &[&dyn ReadableOid]) -> Result<Response, SnmpError> {
        self.write_buffer.clear();
        let request_num = self.request_num;

        let pkt: &[&dyn Serialize] = &[
            &request_num,
            &0u32,
            &0u32,
            &values::NullValueOids(oids.iter().map(|oid| oid.value())),
        ];

        self.serialize_snmp_packet(GET_REQUEST_TAG, pkt);
        self.socket.send(&self.write_buffer).await.unwrap();
        self.recv_packet().await.unwrap();

        Ok(SnmpPacket::deserialize(&mut &self.read_buffer[..self.read_buf_len]).unwrap().pdu)
    }

    pub async fn set(&mut self, vars: &[&dyn WritableOidAndValue]) -> Result<Response, SnmpError> {
        self.write_buffer.clear();
        let request_num = self.request_num;

        let pkt: &[&dyn Serialize] = &[&request_num, &0u32, &0u32, &vars];

        self.serialize_snmp_packet(SET_REQUEST_TAG, pkt);
        self.socket.send(&self.write_buffer).await.unwrap();
        self.recv_packet().await.unwrap();

        Ok(SnmpPacket::deserialize(&mut &self.read_buffer[..self.read_buf_len]).unwrap().pdu)
    }

    async fn recv_packet(&mut self) -> io::Result<()> {
        if let Some(t) = self.timeout {
            self.read_buf_len = timeout(t, self.socket.recv(&mut self.read_buffer[..])).await?;
        } else {
            self.read_buf_len = self.socket.recv(&mut self.read_buffer[..]).await?;
        }
        Ok(())
    }

    fn serialize_snmp_packet(&mut self, tag: u8, contents: &[&dyn Serialize]) {
        let items: &[&dyn Serialize] = &[
            &1u32,
            &values::BorrowedOctetString(&self.community_string.as_bytes()),
            &values::CustomTagSeq::new(tag, contents),
        ];

        items.serialize(&mut self.write_buffer);
    }
}
