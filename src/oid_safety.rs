pub trait ConstOid {
    fn value() -> &'static [u64];
    fn name() -> Option<&'static str> {
        None
    }
}

impl<T: ConstOid + ?Sized> Oid for T {
    fn value(&self) -> &[u64] {
        Self::value()
    }

    fn name(&self) -> Option<&str> {
        Self::name()
    }
}

pub trait Oid {
    fn value(&self) -> &[u64];
    fn name(&self) -> Option<&str> {
        None
    }
}

pub trait ReadableOid: Oid {}
pub trait WritableOid: Oid {}

impl Oid for super::ObjectId {
    fn value(&self) -> &[u64] {
        self.as_slice()
    }
}
impl ReadableOid for super::ObjectId {}
impl WritableOid for super::ObjectId {}

impl Oid for &'_ super::ObjectId {
    fn value(&self) -> &[u64] {
        self.as_slice()
    }
}
impl ReadableOid for &'_ super::ObjectId {}
impl WritableOid for &'_ super::ObjectId {}

impl Oid for &dyn ReadableOid {
    fn value(&self) -> &[u64] {
        (*self).value()
    }
    fn name(&self) -> Option<&str> {
        (*self).name()
    }
}
impl ReadableOid for &dyn ReadableOid {}

impl Oid for &dyn WritableOid {
    fn value(&self) -> &[u64] {
        (*self).value()
    }
    fn name(&self) -> Option<&str> {
        (*self).name()
    }
}
impl WritableOid for &dyn WritableOid {}

impl Oid for Box<dyn ReadableOid> {
    fn value(&self) -> &[u64] {
        (**self).value()
    }
    fn name(&self) -> Option<&str> {
        (**self).name()
    }
}
impl ReadableOid for Box<dyn ReadableOid> {}

impl Oid for Box<dyn WritableOid> {
    fn value(&self) -> &[u64] {
        (**self).value()
    }
    fn name(&self) -> Option<&str> {
        (**self).name()
    }
}
impl WritableOid for Box<dyn WritableOid> {}
