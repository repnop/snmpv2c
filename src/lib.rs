#[cfg(any(feature = "async-std"))]
pub mod asynchronous;
pub mod listener;
pub mod oid_safety;
pub mod req_resp;
pub mod session;
pub mod table;
pub mod values;

pub use self::{req_resp::*, session::*, values::*};

#[macro_export]
macro_rules! varbinds {
    (@inner $s:literal => $v:expr, $($ts:tt)*) => {
        $crate::VariableBinding::new($s.parse::<$crate::Oid>().expect("failed to parse oid"), $v),
        varbinds!(@inner $(ts)*)
    };
    (@inner $e:expr => $v:expr, $($ts:tt)*) => {
        $crate::VariableBinding::new($e, $v),
        varbinds!(@inner $(ts)*)
    };
    (@inner $e:expr, $($ts:tt)*) => {
        $e,
        varbinds!(@inner $(ts)*)
    };
    ($($ts:tt)*) => {{
        &[
            varbinds!(@inner $($ts),*)
        ]
    }}
}

#[macro_export]
macro_rules! oid {
    ($l:literal) => {{
        $l.parse::<$crate::Oid>().expect("failed to parse oid")
    }};
}
