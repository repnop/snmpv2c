use crate::{
    oid_safety::{Oid, ReadableOid, WritableOid},
    req_resp::{
        Response, SnmpPacket, GET_BULK_REQUEST_TAG, GET_NEXT_REQUEST_TAG, GET_REQUEST_TAG,
        SET_REQUEST_TAG,
    },
    values::{self, Deserialize, ObjectId, Serialize, Value, VariableBinding},
};
use std::{
    io::{self},
    net::{ToSocketAddrs, UdpSocket},
    time::Duration,
};

pub type SnmpResult<T> = Result<T, SnmpError>;

pub trait WritableOidAndValue: WritableOid {
    fn oid_bytes(&self) -> &[u64];
    fn oid_value(&self) -> Value;
}

impl<T: WritableOid, U> Oid for (&T, U) {
    fn value(&self) -> &[u64] {
        self.0.value()
    }
}
impl<T: WritableOid, U> WritableOid for (&T, U) {}

impl<T: WritableOid> WritableOidAndValue for (&T, Value) {
    fn oid_bytes(&self) -> &[u64] {
        self.0.value()
    }

    fn oid_value(&self) -> Value {
        self.1.clone()
    }
}

impl Oid for &dyn WritableOidAndValue {
    fn value(&self) -> &[u64] {
        (*self).value()
    }
    fn name(&self) -> Option<&str> {
        (*self).name()
    }
}
impl WritableOid for &dyn WritableOidAndValue {}
impl WritableOidAndValue for &dyn WritableOidAndValue {
    fn oid_bytes(&self) -> &[u64] {
        (*self).oid_bytes()
    }

    fn oid_value(&self) -> Value {
        (*self).oid_value()
    }
}

impl Oid for Box<dyn WritableOidAndValue> {
    fn value(&self) -> &[u64] {
        (**self).value()
    }
    fn name(&self) -> Option<&str> {
        (**self).name()
    }
}
impl WritableOid for Box<dyn WritableOidAndValue> {}
impl WritableOidAndValue for Box<dyn WritableOidAndValue> {
    fn oid_bytes(&self) -> &[u64] {
        (**self).oid_bytes()
    }

    fn oid_value(&self) -> Value {
        (**self).oid_value()
    }
}

#[derive(Debug)]
pub enum SnmpError {
    EmptyResponse,
    DeserializeError(values::DeserializeError),
    IoError(io::Error),
}

impl std::fmt::Display for SnmpError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<values::DeserializeError> for SnmpError {
    fn from(e: values::DeserializeError) -> SnmpError {
        SnmpError::DeserializeError(e)
    }
}

impl From<io::Error> for SnmpError {
    fn from(e: io::Error) -> SnmpError {
        SnmpError::IoError(e)
    }
}

impl std::error::Error for SnmpError {}

pub struct SnmpSession {
    community_string: String,
    read_buffer: Vec<u8>,
    socket: UdpSocket,
    write_buffer: Vec<u8>,
    request_num: u32,
}

impl SnmpSession {
    pub fn new(
        bind_addr: impl ToSocketAddrs,
        peer: impl ToSocketAddrs,
        community_string: &str,
        timeout: impl Into<Option<Duration>>,
    ) -> io::Result<Self> {
        let socket = UdpSocket::bind(bind_addr)?;
        socket.connect(peer)?;
        socket.set_read_timeout(timeout.into())?;

        Ok(Self {
            community_string: community_string.to_string(),
            socket,
            read_buffer: vec![0; 65535],
            write_buffer: Vec::with_capacity(4096),
            request_num: 0,
        })
    }

    pub fn get<T>(&mut self, oids: &[T]) -> SnmpResult<Response>
    where
        T: ReadableOid,
    {
        self.write_buffer.clear();
        let request_num = self.request_num;

        let pkt: &[&dyn Serialize] = &[
            &request_num,
            &0u32,
            &0u32,
            &values::NullValueOids(oids.iter().map(|oid| oid.value())),
        ];

        self.serialize_snmp_packet(GET_REQUEST_TAG, pkt);
        self.socket.send(&self.write_buffer)?;
        let len = self.recv_packet()?;

        Ok(SnmpPacket::deserialize(&mut &self.read_buffer[..len])?.pdu)
    }

    pub fn bulk_get<T>(
        &mut self,
        non_repeaters: u32,
        max_repetitions: u32,
        oids: &[T],
    ) -> SnmpResult<Response>
    where
        T: ReadableOid,
    {
        self.write_buffer.clear();
        let request_num = self.request_num;

        let pkt: &[&dyn Serialize] = &[
            &request_num,
            &non_repeaters,
            &max_repetitions,
            &values::NullValueOids(oids.iter().map(|oid| oid.value())),
        ];

        self.serialize_snmp_packet(GET_BULK_REQUEST_TAG, pkt);
        self.socket.send(&self.write_buffer)?;
        let len = self.recv_packet()?;

        let mut resp: Response = SnmpPacket::deserialize(&mut &self.read_buffer[..len])?.pdu;

        resp.pdu.variable_bindings = resp
            .pdu
            .variable_bindings
            .drain(..)
            .filter(|vb| {
                oids.iter().any(|o| {
                    use values::BorrowedOid;
                    BorrowedOid(vb.name()).in_root(&BorrowedOid(o.value()))
                })
            })
            .collect();

        Ok(resp)
    }

    pub fn get_next<T>(&mut self, oids: &[T]) -> SnmpResult<Response>
    where
        T: ReadableOid,
    {
        self.write_buffer.clear();
        let request_num = self.request_num;

        let pkt: &[&dyn Serialize] = &[
            &request_num,
            &0u32,
            &0u32,
            &values::NullValueOids(oids.iter().map(|oid| oid.value())),
        ];

        self.serialize_snmp_packet(GET_NEXT_REQUEST_TAG, pkt);
        self.socket.send(&self.write_buffer)?;
        let len = self.recv_packet()?;

        Ok(SnmpPacket::deserialize(&mut &self.read_buffer[..len])?.pdu)
    }

    pub fn set<T>(&mut self, vars: &[T]) -> SnmpResult<Response>
    where
        T: WritableOidAndValue,
    {
        self.write_buffer.clear();
        let request_num = self.request_num;

        let pkt: &[&dyn Serialize] = &[&request_num, &0u32, &0u32, &vars];

        self.serialize_snmp_packet(SET_REQUEST_TAG, pkt);
        self.socket.send(&self.write_buffer)?;
        let len = self.recv_packet()?;

        Ok(SnmpPacket::deserialize(&mut &self.read_buffer[..len])?.pdu)
    }

    pub fn walk<T: ReadableOid>(&mut self, oid: T) -> SnmpResult<Vec<VariableBinding>> {
        let base_oid = ObjectId(oid.value().into());
        let mut current_oid = ObjectId(oid.value().into());
        let mut oids = Vec::with_capacity(16);

        loop {
            let response = self.get_next(&[&current_oid])?;
            let mut bindings = response.into_variable_bindings();

            match bindings.first() {
                Some(vb) if vb.value() == &Value::EndOfMibView => break,
                Some(vb) if !vb.name().in_root(&base_oid) => break,
                None => return Err(SnmpError::EmptyResponse),
                _ => {}
            }

            current_oid = bindings[0].name().clone();
            oids.push(bindings.remove(0));
        }

        Ok(oids)
    }

    pub fn lazy_walk<T: ReadableOid>(&mut self, oid: T) -> LazyWalker<'_> {
        LazyWalker {
            session: self,
            base_oid: ObjectId(oid.value().into()),
            current_oid: ObjectId(oid.value().into()),
            done: false,
        }
    }

    pub fn bulk_walk<T: ReadableOid>(
        &mut self,
        max_repetitions: u32,
        oids: &[T],
    ) -> SnmpResult<Vec<VariableBinding>> {
        let mut all_oids = Vec::with_capacity(16);

        for oid in oids {
            let base_oid = ObjectId(oid.value().into());
            let mut current_oid = ObjectId(oid.value().into());
            let reached_beyond = |vb: &VariableBinding| {
                vb.value() == &Value::EndOfMibView || !vb.name().in_root(&base_oid)
            };

            loop {
                let response = self.bulk_get(0, max_repetitions, &[&current_oid])?;
                let mut bindings = response.into_variable_bindings();
                let len = bindings.len();
                let end = bindings.iter().position(reached_beyond).unwrap_or(len);

                if let Some(vb) = bindings.last() {
                    current_oid = vb.name().clone()
                }

                all_oids.extend(bindings.drain(..end));

                if end != len || len == 0 {
                    break;
                }
            }
        }

        Ok(all_oids)
    }

    fn recv_packet(&mut self) -> Result<usize, io::Error> {
        self.socket.recv(&mut self.read_buffer[..])
    }

    fn serialize_snmp_packet(&mut self, tag: u8, contents: &[&dyn Serialize]) {
        self.request_num += 1;
        let items: &[&dyn Serialize] = &[
            &1u32,
            &values::BorrowedOctetString(&self.community_string.as_bytes()),
            &values::CustomTagSeq::new(tag, contents),
        ];

        items.serialize(&mut self.write_buffer);
    }
}

pub struct LazyWalker<'a> {
    session: &'a mut SnmpSession,
    base_oid: ObjectId,
    current_oid: ObjectId,
    done: bool,
}

impl Iterator for LazyWalker<'_> {
    type Item = SnmpResult<VariableBinding>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            None
        } else {
            let response = match self.session.get_next(&[&self.current_oid]) {
                Ok(r) => r,
                Err(e) => {
                    self.done = true;
                    return Some(Err(e));
                }
            };

            let mut bindings = response.into_variable_bindings();

            match bindings.first() {
                Some(vb) if vb.value() == &Value::EndOfMibView => {
                    self.done = true;
                    return None;
                }
                Some(vb) if !vb.name().in_root(&self.base_oid) => {
                    self.done = true;
                    return None;
                }
                None => {
                    self.done = true;
                    return Some(Err(SnmpError::EmptyResponse));
                }
                _ => {}
            }

            self.current_oid = bindings[0].name().clone();

            Some(Ok(bindings.remove(0)))
        }
    }
}

impl std::iter::FusedIterator for LazyWalker<'_> {}
