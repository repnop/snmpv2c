use crate::{
    oid_safety::{ConstOid, Oid},
    values::{
        self, Deserialize, DeserializeError, ObjectId, ReadExt, Value, VariableBinding,
        ASN1_CONSTRUCTED, ASN1_CONTEXT_SPECIFIC, SEQUENCE_TAG,
    },
};
use std::convert::TryFrom;

pub(crate) const GET_REQUEST_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED;
pub(crate) const GET_NEXT_REQUEST_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED | 1;
pub(crate) const RESPONSE_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED | 2;
pub(crate) const SET_REQUEST_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED | 3;
pub(crate) const GET_BULK_REQUEST_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED | 5;
pub(crate) const INFORM_REQUEST_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED | 6;
pub(crate) const SNMP_V2_TRAP_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED | 7;
pub(crate) const REPORT_TAG: u8 = ASN1_CONTEXT_SPECIFIC | ASN1_CONSTRUCTED | 8;

pub type PduPacket = SnmpPacket<Pdus>;

#[derive(Debug, Clone, PartialEq)]
pub struct SnmpPacket<T: Deserialize> {
    pub version: u32,
    pub community_string: values::OctetString,
    pub pdu: T,
}

impl<T: Deserialize> Deserialize for SnmpPacket<T> {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(SEQUENCE_TAG)?;
        let _ = values::Length::deserialize(bytes)?;
        let version = u32::deserialize(bytes)?;

        if version != 1 {
            return Err(DeserializeError::BadVersion(version));
        }

        let community_string = values::OctetString::deserialize(bytes)?;
        let pdu = T::deserialize(bytes)?;

        Ok(Self { version, community_string, pdu })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct BulkPdu {
    request_id: i32,
    non_repeaters: i32,
    max_repititions: i32,
    variable_bindings: Vec<VariableBinding>,
}

impl Deserialize for BulkPdu {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        let request_id = i32::deserialize(bytes)?;
        let non_repeaters = i32::deserialize(bytes)?;
        let max_repititions = i32::deserialize(bytes)?;
        let variable_bindings = Vec::deserialize(bytes)?;

        Ok(Self { request_id, non_repeaters, max_repititions, variable_bindings })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Pdu {
    request_id: i32,
    error_status: ErrorStatus,
    error_index: usize,
    pub(crate) variable_bindings: Vec<VariableBinding>,
}

impl Pdu {
    pub fn request_id(&self) -> i32 {
        self.request_id
    }

    pub fn error_status(&self) -> ErrorStatus {
        self.error_status
    }

    pub fn error_index(&self) -> usize {
        self.error_index
    }

    pub fn names(&self) -> impl Iterator<Item = &'_ ObjectId> {
        self.variable_bindings.iter().map(|vb| vb.name())
    }

    pub fn values(&self) -> impl Iterator<Item = &'_ Value> {
        self.variable_bindings.iter().map(|vb| vb.value())
    }

    pub fn variable_bindings(&self) -> &[VariableBinding] {
        &self.variable_bindings
    }
}

impl Deserialize for Pdu {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        let request_id = i32::deserialize(bytes)?;
        let error_status = ErrorStatus::try_from(i32::deserialize(bytes)?)?;
        let error_index = u64::deserialize(bytes)? as usize;
        let variable_bindings = Vec::deserialize(bytes)?;

        Ok(Self { request_id, error_status, error_index, variable_bindings })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Pdus {
    GetRequest(Pdu),
    GetNextRequest(Pdu),
    Response(Pdu),
    SetRequest(Pdu),
    GetBulkRequest(BulkPdu),
    InformRequest(Pdu),
    SnmpV2Trap(Pdu),
    Report(Pdu),
}

impl Pdus {
    pub fn get_request(&self) -> Option<&Pdu> {
        match self {
            Pdus::GetRequest(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn get_next_request(&self) -> Option<&Pdu> {
        match self {
            Pdus::GetNextRequest(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn response(&self) -> Option<&Pdu> {
        match self {
            Pdus::Response(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn set_request(&self) -> Option<&Pdu> {
        match self {
            Pdus::SetRequest(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn get_bulk_request(&self) -> Option<&BulkPdu> {
        match self {
            Pdus::GetBulkRequest(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn inform_request(&self) -> Option<&Pdu> {
        match self {
            Pdus::InformRequest(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn trap(&self) -> Option<&Pdu> {
        match self {
            Pdus::SnmpV2Trap(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn report(&self) -> Option<&Pdu> {
        match self {
            Pdus::Report(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn pdu(&self) -> Option<&Pdu> {
        match self {
            Pdus::GetRequest(pdu) => Some(pdu),
            Pdus::GetNextRequest(pdu) => Some(pdu),
            Pdus::Response(pdu) => Some(pdu),
            Pdus::SetRequest(pdu) => Some(pdu),
            Pdus::GetBulkRequest(_) => None,
            Pdus::InformRequest(pdu) => Some(pdu),
            Pdus::SnmpV2Trap(pdu) => Some(pdu),
            Pdus::Report(pdu) => Some(pdu),
        }
    }
}

impl Deserialize for Pdus {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        let tag = bytes[0];

        if tag == GET_BULK_REQUEST_TAG {
            bytes.byte();
            let length = values::Length::deserialize(bytes)?;
            let pkt_slice = &mut bytes.slice(length);
            return Ok(Pdus::GetBulkRequest(BulkPdu::deserialize(pkt_slice)?));
        }

        let constructor = match tag {
            GET_REQUEST_TAG => Pdus::GetRequest,
            GET_NEXT_REQUEST_TAG => Pdus::GetNextRequest,
            RESPONSE_TAG => Pdus::Response,
            SET_REQUEST_TAG => Pdus::SetRequest,
            INFORM_REQUEST_TAG => Pdus::InformRequest,
            SNMP_V2_TRAP_TAG => Pdus::SnmpV2Trap,
            REPORT_TAG => Pdus::Report,
            _ => return Err(DeserializeError::UnknownTag(tag)),
        };

        bytes.byte();
        let length = values::Length::deserialize(bytes)?;
        let pkt_slice = &mut bytes.slice(length);

        Ok(constructor(Pdu::deserialize(pkt_slice)?))
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(i32)]
pub enum ErrorStatus {
    NoError = 0,
    TooBig = 1,
    NoSuchName = 2,
    BadValue = 3,
    ReadOnly = 4,
    GenErr = 5,
    NoAccess = 6,
    WrongType = 7,
    WrongLength = 8,
    WrongEncoding = 9,
    WrongValue = 10,
    NoCreation = 11,
    InconsistentValue = 12,
    ResourceUnavailable = 13,
    CommitFailed = 14,
    UndoFailed = 15,
    AuthroizationError = 16,
    NotWritable = 17,
    InconsistentName = 18,
}

impl TryFrom<i32> for ErrorStatus {
    type Error = DeserializeError;

    fn try_from(n: i32) -> Result<Self, Self::Error> {
        match n {
            0 => Ok(ErrorStatus::NoError),
            1 => Ok(ErrorStatus::TooBig),
            2 => Ok(ErrorStatus::NoSuchName),
            3 => Ok(ErrorStatus::BadValue),
            4 => Ok(ErrorStatus::ReadOnly),
            5 => Ok(ErrorStatus::GenErr),
            6 => Ok(ErrorStatus::NoAccess),
            7 => Ok(ErrorStatus::WrongType),
            8 => Ok(ErrorStatus::WrongLength),
            9 => Ok(ErrorStatus::WrongEncoding),
            10 => Ok(ErrorStatus::WrongValue),
            11 => Ok(ErrorStatus::NoCreation),
            12 => Ok(ErrorStatus::InconsistentValue),
            13 => Ok(ErrorStatus::ResourceUnavailable),
            14 => Ok(ErrorStatus::CommitFailed),
            15 => Ok(ErrorStatus::UndoFailed),
            16 => Ok(ErrorStatus::AuthroizationError),
            17 => Ok(ErrorStatus::NotWritable),
            18 => Ok(ErrorStatus::InconsistentName),
            _ => Err(DeserializeError::InvalidValue),
        }
    }
}

pub struct GetRequest {}

pub enum ExtractResult<T: ConstOid + TryFrom<Value>> {
    ConversionError(<T as TryFrom<Value>>::Error),
    NotFound,
    Ok(T),
}

impl<T: ConstOid + TryFrom<Value>> ExtractResult<T>
where
    <T as TryFrom<Value>>::Error: std::fmt::Debug,
{
    pub fn unwrap(self) -> T {
        match self {
            ExtractResult::Ok(t) => t,
            ExtractResult::ConversionError(e) => panic!("ExtractResult: Conversion error: {:?}", e),
            ExtractResult::NotFound => {
                panic!("ExtractResult: Value not found: {}", values::BorrowedOid(T::value()))
            }
        }
    }
}

#[derive(Debug)]
pub struct Response {
    pub(crate) pdu: Pdu,
}

impl Response {
    pub fn pdu(&self) -> &Pdu {
        &self.pdu
    }

    pub fn variable_bindings(&self) -> &[VariableBinding] {
        &self.pdu.variable_bindings
    }

    pub fn into_variable_bindings(self) -> Vec<VariableBinding> {
        self.pdu.variable_bindings
    }

    pub fn get(&self, oid: &dyn Oid) -> Option<&values::Value> {
        self.pdu
            .variable_bindings
            .iter()
            .find(|vb| vb.name().as_slice() == oid.value())
            .map(|vb| vb.value())
    }

    pub fn extract<T: ConstOid + TryFrom<Value>>(&self) -> ExtractResult<T> {
        match self.get(&values::BorrowedOid(T::value())).cloned().map(TryFrom::try_from) {
            Some(Ok(t)) => ExtractResult::Ok(t),
            Some(Err(e)) => ExtractResult::ConversionError(e),
            None => ExtractResult::NotFound,
        }
    }
}

impl Deserialize for Response {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(RESPONSE_TAG)?;
        let length = values::Length::deserialize(bytes)?;
        let pkt_slice = &mut bytes.slice(length);

        Ok(Self { pdu: Pdu::deserialize(pkt_slice)? })
    }
}
