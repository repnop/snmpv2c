use crate::{
    req_resp::{Pdus, SnmpPacket},
    session::SnmpResult,
    values::Deserialize,
};
use std::{
    io,
    net::{SocketAddr, ToSocketAddrs, UdpSocket},
};

pub struct SnmpListener {
    socket: UdpSocket,
    buffer: Vec<u8>,
}

impl SnmpListener {
    pub fn bind<A: ToSocketAddrs>(addr: A) -> io::Result<Self> {
        let socket = UdpSocket::bind(addr)?;
        let buffer = vec![0; 65535];

        Ok(Self { socket, buffer })
    }

    pub fn accept(&mut self) -> SnmpResult<(SnmpPacket<Pdus>, SocketAddr)> {
        let (len, sockaddr) = self.socket.recv_from(&mut self.buffer[..])?;

        Ok((SnmpPacket::deserialize(&mut &self.buffer[..len])?, sockaddr))
    }

    pub fn incoming(&mut self) -> Incoming<'_> {
        Incoming { listener: self }
    }

    pub fn incoming_with_addrs(&mut self) -> IncomingWithAddrs<'_> {
        IncomingWithAddrs { listener: self }
    }
}

pub struct Incoming<'a> {
    listener: &'a mut SnmpListener,
}

impl Iterator for Incoming<'_> {
    type Item = SnmpResult<SnmpPacket<Pdus>>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.listener.accept().map(|(pkt, _)| pkt))
    }
}

pub struct IncomingWithAddrs<'a> {
    listener: &'a mut SnmpListener,
}

impl Iterator for IncomingWithAddrs<'_> {
    type Item = SnmpResult<(SnmpPacket<Pdus>, SocketAddr)>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.listener.accept())
    }
}
