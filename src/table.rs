#![allow(dead_code)]

use crate::{
    session::{SnmpResult, SnmpSession},
    values::{ObjectId, Value},
};
use std::collections::HashMap;

pub struct SnmpTable<'a> {
    session: &'a mut SnmpSession,
    table_oid: ObjectId,
    column_map: HashMap<u64, &'static str>,
}

impl<'a> SnmpTable<'a> {
    pub fn new(
        session: &'a mut SnmpSession,
        table_oid: ObjectId,
        column_map: HashMap<u64, &'static str>,
    ) -> Self {
        Self { session, table_oid, column_map }
    }

    pub fn rows(&mut self) -> SnmpResult<()> {
        let col_index = self.table_oid.iter().count();
        let table = self.session.walk(&self.table_oid)?;
        let mut rows = HashMap::new();

        for entry in &table {
            let mut entry_iter = entry.name().iter();
            let col_index = entry_iter.by_ref().nth(col_index).unwrap();
            if col_index == 1 {
                rows.insert(entry_iter.next().unwrap(), HashMap::new());
            } else if let Some(e) = rows.get_mut(&entry_iter.next().unwrap()) {
                e.insert(col_index, entry.value());
            }
        }

        let mut entries = rows
            .iter()
            .map(|(k, v)| {
                (k, {
                    let mut v = v.iter().map(|(k, v)| (k, (*v).clone())).collect::<Vec<_>>();
                    v.sort_by_key(|e| e.0);

                    v
                })
            })
            .collect::<Vec<_>>();
        entries.sort_by_key(|e| e.0);

        for entry in entries {
            print!("{}: ", entry.0);
            for inner in entry.1 {
                print!("{:?} | ", inner.1);
            }
            println!();
        }

        Ok(())
    }
}

pub struct Row {
    columns: Vec<Value>,
    index: u64,
}
