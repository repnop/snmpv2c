use std::net::Ipv4Addr;

pub(crate) const ASN1_APPLICATION: u8 = 0b0100_0000;
pub(crate) const ASN1_CONTEXT_SPECIFIC: u8 = 0b1000_0000;
pub(crate) const ASN1_CONSTRUCTED: u8 = 0b0010_0000;
pub(crate) const ASN1_UNIVERSAL: u8 = 0b0000_0000;

pub(crate) const SEQUENCE_TAG: u8 = ASN1_UNIVERSAL | ASN1_CONSTRUCTED | 16;
pub(crate) const INTEGER_TAG: u8 = 0x02;
pub(crate) const NULL_TAG: u8 = 0x05;
pub(crate) const OCTET_STRING_TAG: u8 = 0x04;
pub(crate) const OBJECT_IDENTIFIER_TAG: u8 = 0x06;

pub(crate) const IP_ADDRESS_TAG: u8 = ASN1_APPLICATION;
pub(crate) const COUNTER32_TAG: u8 = ASN1_APPLICATION | 1;
pub(crate) const UNSIGNED32_TAG: u8 = ASN1_APPLICATION | 2;
pub(crate) const TIME_TICKS_TAG: u8 = ASN1_APPLICATION | 3;
pub(crate) const OPAQUE_TAG: u8 = ASN1_APPLICATION | 4;
pub(crate) const COUNTER64_TAG: u8 = ASN1_APPLICATION | 6;

pub(crate) const NO_SUCH_OBJECT_TAG: u8 = ASN1_CONTEXT_SPECIFIC;
pub(crate) const NO_SUCH_INSTANCE_TAG: u8 = ASN1_CONTEXT_SPECIFIC | 1;
pub(crate) const END_OF_MIB_VIEW_TAG: u8 = ASN1_CONTEXT_SPECIFIC | 2;

pub(crate) trait VecExt {
    fn write(&mut self, slice: &[u8]);
    fn write_byte(&mut self, byte: u8);
}

impl VecExt for Vec<u8> {
    fn write(&mut self, slice: &[u8]) {
        use std::io::Write;
        self.write_all(slice).unwrap();
    }
    fn write_byte(&mut self, byte: u8) {
        self.write(&[byte]);
    }
}

pub trait ReadExt {
    fn byte(&mut self) -> u8;
    fn uint(&mut self, size: Length) -> u64;
    fn int(&mut self, size: Length) -> i64;
    fn slice(&mut self, size: Length) -> &[u8];
    fn tag(&mut self, expected: u8) -> Result<u8, DeserializeError>;
}

impl ReadExt for &'_ [u8] {
    fn byte(&mut self) -> u8 {
        let byte = self[0];
        *self = &self[1..];
        byte
    }

    fn uint(&mut self, size: Length) -> u64 {
        let size = size.0 as usize;
        debug_assert!(size <= 8);

        if size == 0 {
            return 0;
        }

        let mut bytes = [0; 8];
        bytes[8 - size..][..size].copy_from_slice(&self[..size]);

        *self = &self[size..];

        u64::from_be_bytes(bytes)
    }

    fn int(&mut self, size: Length) -> i64 {
        let size = size.0 as usize;
        debug_assert!(size <= 8);

        if size == 0 {
            return 0;
        }

        let mut bytes = if self[0] & 0x80 == 0x80 { [0xFF; 8] } else { [0x00; 8] };

        bytes[8 - size..][..size].copy_from_slice(&self[..size]);

        *self = &self[size..];

        i64::from_be_bytes(bytes)
    }

    fn slice(&mut self, size: Length) -> &[u8] {
        let size = size.0 as usize;

        if size == 0 {
            return &[];
        }

        let slice = &self[..size];
        *self = &self[size..];

        slice
    }

    fn tag(&mut self, expected: u8) -> Result<u8, DeserializeError> {
        let tag = self.byte();

        if tag != expected {
            Err(DeserializeError::IncorrectTag(tag, expected))
        } else {
            Ok(tag)
        }
    }
}

pub trait Serialize {
    fn serialize(&self, buffer: &mut Vec<u8>);
}

pub trait Deserialize: Sized {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError>;
}

#[derive(Debug)]
pub enum DeserializeError {
    IncorrectTag(u8, u8),
    IndefiniteLength,
    InvalidValue,
    UnknownTag(u8),
    BadVersion(u32),
}

#[derive(Debug, Clone, Copy)]
pub struct Length(u64);

impl Length {
    pub fn new(len: u64) -> Self {
        Self(len)
    }
}

impl Deserialize for Length {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        const MASK_LENGTH_DEFINITE: u8 = 0b1000_0000;
        const LENGTH_DEFINITE_SHORT: u8 = 0b0000_0000;

        let length_type = bytes.byte();
        let length_value = u64::from(length_type & 0x7F);

        match length_type & MASK_LENGTH_DEFINITE {
            LENGTH_DEFINITE_SHORT => Ok(Self::new(length_value)),
            _ if length_value > 0 => Ok(Self::new(bytes.uint(Length::new(length_value)))),
            _ => Err(DeserializeError::IndefiniteLength),
        }
    }
}

impl Serialize for Length {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        if self.0 > 127 {
            buffer.write_byte(0x88);
            let ns = self.0.to_be_bytes();
            let start = ns.iter().copied().position(|n| n != 0).unwrap_or(7);
            buffer.write(&ns[start..]);
        } else {
            buffer.write_byte(self.0 as u8);
        }
    }
}

impl std::ops::Add for Length {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Self(self.0 + rhs.0)
    }
}

impl Deserialize for i32 {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(INTEGER_TAG)?;
        let length = Length::deserialize(bytes)?;
        Ok(bytes.int(length) as i32)
    }
}

impl Serialize for i32 {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(INTEGER_TAG);
        let ns = self.to_be_bytes();
        let start = if self.is_negative() {
            0
        } else {
            let start = ns.iter().copied().position(|n| n != 0).unwrap_or(3);

            if start != 0 && ns[start] & 0x80 == 0x80 {
                start - 1
            } else {
                start
            }
        };

        let length = Length::new(4 - start as u64);

        length.serialize(buffer);
        buffer.write(&ns[start..]);
    }
}

impl Deserialize for u32 {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(INTEGER_TAG)?;
        let length = Length::deserialize(bytes)?;
        Ok(bytes.uint(length) as u32)
    }
}

impl Serialize for u32 {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        (*self as i32).serialize(buffer);
    }
}

impl Serialize for i64 {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(INTEGER_TAG);
        let ns = self.to_be_bytes();
        let start = if self.is_negative() {
            0
        } else {
            let start = ns.iter().copied().position(|n| n != 0).unwrap_or(7);

            if start != 0 && ns[start] & 0x80 == 0x80 {
                start - 1
            } else {
                start
            }
        };

        let length = Length::new(4 - start as u64);

        length.serialize(buffer);
        buffer.write(&ns[start..]);
    }
}

impl Deserialize for u64 {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(INTEGER_TAG)?;
        let length = Length::deserialize(bytes)?;
        Ok(bytes.uint(length) as u64)
    }
}

impl Serialize for u64 {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        (*self as i64).serialize(buffer);
    }
}

#[derive(Clone, PartialEq)]
pub struct OctetString(Vec<u8>);

impl OctetString {
    pub fn as_bytes(&self) -> &[u8] {
        &self.0
    }

    /// Attempts to convert the `OctetString` to `&str`, returning `None` on
    /// failure
    pub fn as_str(&self) -> Option<&str> {
        std::str::from_utf8(&self.0).ok()
    }

    pub fn to_lossy_string(&self) -> std::borrow::Cow<'_, str> {
        String::from_utf8_lossy(&self.0)
    }
}

impl std::fmt::Debug for OctetString {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "OctetString({:?})", String::from_utf8_lossy(&self.0))
    }
}

impl Deserialize for OctetString {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(OCTET_STRING_TAG)?;
        let length = Length::deserialize(bytes)?;

        Ok(Self(bytes.slice(length).to_vec()))
    }
}

impl Serialize for OctetString {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(OCTET_STRING_TAG);
        Length::new(self.0.len() as u64).serialize(buffer);
        buffer.write(&self.0);
    }
}

impl Deserialize for Ipv4Addr {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(IP_ADDRESS_TAG)?;
        let (a, b, c, d) = (bytes.byte(), bytes.byte(), bytes.byte(), bytes.byte());

        Ok(Self::new(a, b, c, d))
    }
}

// #[macro_export]
// macro_rules! oid {
//
// }

pub type Oid = ObjectId;

#[derive(Clone, PartialEq)]
pub struct ObjectId(pub(crate) Vec<u64>);

impl ObjectId {
    pub const fn new() -> Self {
        Self(Vec::new())
    }

    pub fn as_slice(&self) -> &[u64] {
        &self.0
    }

    pub fn iter(&self) -> impl Iterator<Item = u64> + '_ {
        self.0.iter().copied()
    }

    pub fn in_root(&self, root: &Self) -> bool {
        if self.0.len() < root.0.len() {
            false
        } else {
            self.0.iter().copied().zip(root.0.iter().copied()).all(|(a, b)| a == b)
        }
    }

    pub fn push(&mut self, oid_piece: u64) {
        self.0.push(oid_piece);
    }

    pub fn pop(&mut self) -> Option<u64> {
        self.0.pop()
    }

    pub fn at(&self, position: usize) -> Option<u64> {
        self.iter().nth(position)
    }

    pub fn encoded(&self) -> Vec<u8> {
        let mut encoded = Vec::with_capacity(self.0.len().next_power_of_two());
        self.encode_bytes_into(&mut encoded);
        encoded
    }

    pub(crate) fn encode_bytes_into(&self, b: &mut Vec<u8>) {
        b.push(self.0[0] as u8 * 40 + self.0[1] as u8);
        for n in self.iter().skip(2) {
            let i = (0..=9u64).rev().find(|i| n & (0x7F << (i * 7)) > 0).unwrap_or(0);
            for j in 0..=i {
                let x = ((n >> ((i - j) * 7)) & 127) | 128;
                b.push(x as u8);
            }
            *b.last_mut().unwrap() ^= 0x80;
        }
    }

    //fn after(&self, position: usize) -> Option<BorrowedOid> {
    //
    //}

    //fn push_vlq_u64(&mut self, n: u64) {
    //    let i = (0..=9u64).rev().find(|i| n & (0x7F << (i * 7)) > 0).unwrap_or(0);
    //
    //    for j in 0..=i {
    //        let x = ((n >> ((i - j) * 7)) & 127) | 128;
    //        self.0.push(x as u8);
    //    }
    //
    //    *self.0.last_mut().unwrap() ^= 0x80;
    //}
}

impl Deserialize for ObjectId {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(OBJECT_IDENTIFIER_TAG)?;
        let length = Length::deserialize(bytes)?;
        let mut slice = bytes.slice(length);
        let mut oid_pieces = Vec::with_capacity(slice.len());

        let first = slice.byte() as u64;
        oid_pieces.push(first / 40);
        oid_pieces.push(first % 40);

        while !slice.is_empty() {
            let mut n = 0u64;

            'inner: loop {
                n <<= 7;
                let byte = slice.byte();

                if byte & 0x80 == 0x00 {
                    oid_pieces.push(n | (byte as u64));
                    break 'inner;
                }

                n |= (byte & 0x7F) as u64;
            }
        }

        Ok(Self(oid_pieces))
    }
}

impl Serialize for ObjectId {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(OBJECT_IDENTIFIER_TAG);
        buffer.write_byte(0x82); // long length, 2 bytes

        let seq_idx = buffer.len();
        let len_start = seq_idx + 2;
        buffer.write(&[0, 0]); // premade 16-bit length value

        self.encode_bytes_into(buffer);

        let seq_len = (buffer.len() - len_start) as u16;
        buffer[seq_idx..seq_idx + 2].copy_from_slice(&seq_len.to_be_bytes()[..]);
    }
}

impl std::fmt::Debug for ObjectId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.0.is_empty() {
            write!(f, "ObjectId(<empty>)")
        } else {
            let mut iter = self.iter();

            write!(f, "ObjectId({}", iter.next().expect("no first oid value"))?;

            for oid in iter {
                write!(f, ".{}", oid)?;
            }

            write!(f, ")")?;

            Ok(())
        }
    }
}

impl std::fmt::Display for ObjectId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.0.is_empty() {
            write!(f, "<empty object id>")
        } else {
            let mut iter = self.iter();

            write!(f, "{}", iter.next().expect("no first oid value"))?;

            for oid in iter {
                write!(f, ".{}", oid)?;
            }

            Ok(())
        }
    }
}

impl std::ops::Deref for ObjectId {
    type Target = [u64];

    fn deref(&self) -> &[u64] {
        &self.0
    }
}

impl ::std::str::FromStr for ObjectId {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut oids = Self(Vec::new());

        let mut parts = s.split('.');

        let first = parts.next().expect("Cannot construct OID from empty string");
        let first = first.parse::<u8>()? as u64;

        assert!(first >= 1 && first <= 3, "Invalid root value in OID");
        oids.0.push(first);

        let second = parts
            .next()
            .expect("Cannot construct OID from one integer value, must be at least a pair");
        oids.0.push(second.parse::<u64>()?);

        for part in parts {
            let oid = part.parse::<u64>()?;
            oids.0.push(oid);
        }

        Ok(oids)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct VariableBinding {
    name: ObjectId,
    value: Value,
}

impl VariableBinding {
    pub fn new(name: ObjectId, value: Value) -> Self {
        Self { name, value }
    }

    pub fn into_name(self) -> ObjectId {
        self.name
    }

    pub fn into_value(self) -> Value {
        self.value
    }

    pub fn into_pair(self) -> (ObjectId, Value) {
        (self.name, self.value)
    }

    pub fn name(&self) -> &ObjectId {
        &self.name
    }

    pub fn value(&self) -> &Value {
        &self.value
    }
}

impl Deserialize for VariableBinding {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(SEQUENCE_TAG)?;
        let slice_len = Length::deserialize(bytes)?;
        let slice = &mut bytes.slice(slice_len);
        let name = ObjectId::deserialize(slice)?;
        let value = Value::deserialize(slice)?;

        Ok(Self { name, value })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Counter32(u32),
    Counter64(u64),
    EndOfMibView,
    Integer(i32),
    Ipv4Address(Ipv4Addr),
    NoSuchInstance,
    NoSuchObject,
    Null,
    ObjectId(ObjectId),
    OctetString(OctetString),
    Opaque(Vec<u8>),
    TimeTicks(u32),
    Unsigned32(u32),
}

impl Value {
    pub fn counter32(&self) -> Option<u32> {
        match self {
            Value::Counter32(n) => Some(*n),
            _ => None,
        }
    }

    pub fn counter64(&self) -> Option<u64> {
        match self {
            Value::Counter64(n) => Some(*n),
            _ => None,
        }
    }

    pub fn integer(&self) -> Option<i32> {
        match self {
            Value::Integer(n) => Some(*n),
            _ => None,
        }
    }

    pub fn ipv4(&self) -> Option<Ipv4Addr> {
        match self {
            Value::Ipv4Address(ip) => Some(*ip),
            _ => None,
        }
    }

    pub fn octet_string(&self) -> Option<&OctetString> {
        match self {
            Value::OctetString(oc) => Some(oc),
            _ => None,
        }
    }
}

impl Deserialize for Value {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        // Use the index as a peek so we know what to decode
        let tag = bytes[0];

        let decoded = match tag {
            INTEGER_TAG => Value::Integer(i32::deserialize(bytes)?),
            OCTET_STRING_TAG => Value::OctetString(OctetString::deserialize(bytes)?),
            OBJECT_IDENTIFIER_TAG => Value::ObjectId(ObjectId::deserialize(bytes)?),
            IP_ADDRESS_TAG => Value::Ipv4Address(Ipv4Addr::deserialize(bytes)?),
            COUNTER32_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                let val = bytes.uint(length) as u32;

                Value::Counter32(val)
            }
            COUNTER64_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                let val = bytes.uint(length);

                Value::Counter64(val)
            }
            UNSIGNED32_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                let val = bytes.uint(length) as u32;

                Value::Unsigned32(val)
            }
            NULL_TAG => {
                // Ideally this should always be `0x05 0x00` but just in case...
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                bytes.uint(length);

                Value::Null
            }
            OPAQUE_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;

                Value::Opaque(bytes.slice(length).to_vec())
            }
            TIME_TICKS_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                let val = bytes.uint(length) as u32;

                Value::TimeTicks(val)
            }
            NO_SUCH_OBJECT_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                bytes.uint(length);

                Value::NoSuchObject
            }
            NO_SUCH_INSTANCE_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                bytes.uint(length);

                Value::NoSuchInstance
            }
            END_OF_MIB_VIEW_TAG => {
                bytes.byte();
                let length = Length::deserialize(bytes)?;
                bytes.uint(length);

                Value::EndOfMibView
            }
            _ => return Err(DeserializeError::UnknownTag(tag)),
        };

        Ok(decoded)
    }
}

impl Serialize for Value {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        match self {
            Value::Counter32(n) => {
                buffer.write_byte(COUNTER32_TAG);
                let bytes = n.to_be_bytes();
                let start = bytes.iter().copied().position(|n| n != 0).unwrap_or(3);
                Length::new(4 - start as u64).serialize(buffer);
                buffer.write(&bytes[start..]);
            }
            Value::Counter64(n) => {
                buffer.write_byte(COUNTER64_TAG);
                let bytes = n.to_be_bytes();
                let start = bytes.iter().copied().position(|n| n != 0).unwrap_or(7);
                Length::new(8 - start as u64).serialize(buffer);
                buffer.write(&bytes[start..]);
            }
            Value::Integer(n) => {
                n.serialize(buffer);
            }
            Value::Ipv4Address(addr) => {
                buffer.write_byte(IP_ADDRESS_TAG);
                Length::new(4).serialize(buffer);
                buffer.write(&addr.octets()[..]);
            }
            Value::Null => {
                buffer.write_byte(NULL_TAG);
                Length::new(0).serialize(buffer);
            }
            Value::ObjectId(oid) => {
                oid.serialize(buffer);
            }
            Value::OctetString(os) => {
                os.serialize(buffer);
            }
            Value::Opaque(v) => {
                buffer.write_byte(OPAQUE_TAG);
                Length::new(v.len() as u64).serialize(buffer);
                buffer.write(&v);
            }
            Value::TimeTicks(n) => {
                buffer.write_byte(TIME_TICKS_TAG);
                let bytes = n.to_be_bytes();
                let start = bytes.iter().copied().position(|n| n != 0).unwrap_or(3);
                Length::new(4 - start as u64).serialize(buffer);
                buffer.write(&bytes[start..]);
            }
            Value::Unsigned32(n) => {
                buffer.write_byte(UNSIGNED32_TAG);
                let bytes = n.to_be_bytes();
                let start = bytes.iter().copied().position(|n| n != 0).unwrap_or(3);
                Length::new(4 - start as u64).serialize(buffer);
                buffer.write(&bytes[start..]);
            }
            Value::NoSuchObject => {
                buffer.write_byte(NO_SUCH_OBJECT_TAG);
                Length::new(0).serialize(buffer);
            }
            Value::NoSuchInstance => {
                buffer.write_byte(NO_SUCH_INSTANCE_TAG);
                Length::new(0).serialize(buffer);
            }
            Value::EndOfMibView => {
                buffer.write_byte(END_OF_MIB_VIEW_TAG);
                Length::new(0).serialize(buffer);
            }
        }
    }
}

impl Serialize for [&'_ dyn Serialize] {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(SEQUENCE_TAG);
        buffer.write_byte(0x82); // long length, 2 bytes

        let seq_idx = buffer.len();
        buffer.write(&[0, 0]); // premade 16-bit length value
        let len_start = seq_idx + 2;
        for obj in self {
            obj.serialize(buffer);
        }

        let seq_len = (buffer.len() - len_start) as u16;
        buffer[seq_idx..seq_idx + 2].copy_from_slice(&seq_len.to_be_bytes()[..]);
    }
}

/// Sequence of values
impl<T: Deserialize> Deserialize for Vec<T> {
    fn deserialize(bytes: &mut &[u8]) -> Result<Self, DeserializeError> {
        bytes.tag(SEQUENCE_TAG)?;

        let mut seq = Vec::with_capacity(8);
        let seq_len = Length::deserialize(bytes)?;
        let seq_slice = &mut bytes.slice(seq_len);

        while !seq_slice.is_empty() {
            seq.push(T::deserialize(seq_slice)?);
        }

        Ok(seq)
    }
}

pub(crate) struct BorrowedOid<'a>(pub(crate) &'a [u64]);

impl<'a> BorrowedOid<'a> {
    pub(crate) fn in_root(&self, root: &Self) -> bool {
        if self.0.len() < root.0.len() {
            false
        } else {
            self.0.iter().copied().zip(root.0.iter().copied()).all(|(a, b)| a == b)
        }
    }

    pub(crate) fn encoded(&self) -> Vec<u8> {
        let mut encoded = Vec::with_capacity(self.0.len().next_power_of_two());
        self.encode_bytes_into(&mut encoded);
        encoded
    }

    pub(crate) fn encode_bytes_into(&self, b: &mut Vec<u8>) {
        b.push(self.0[0] as u8 * 40 + self.0[1] as u8);
        for n in self.0.iter().skip(2) {
            let i = (0..=9u64).rev().find(|i| n & (0x7F << (i * 7)) > 0).unwrap_or(0);
            for j in 0..=i {
                let x = ((n >> ((i - j) * 7)) & 127) | 128;
                b.push(x as u8);
            }
            *b.last_mut().unwrap() ^= 0x80;
        }
    }
}

impl Serialize for BorrowedOid<'_> {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(OBJECT_IDENTIFIER_TAG);
        buffer.write_byte(0x82); // long length, 2 bytes

        let seq_idx = buffer.len();
        let len_start = seq_idx + 2;
        buffer.write(&[0, 0]); // premade 16-bit length value

        self.encode_bytes_into(buffer);

        let seq_len = (buffer.len() - len_start) as u16;
        buffer[seq_idx..seq_idx + 2].copy_from_slice(&seq_len.to_be_bytes()[..]);
    }
}

impl crate::oid_safety::Oid for BorrowedOid<'_> {
    fn value(&self) -> &[u64] {
        self.0
    }
}

impl std::fmt::Display for BorrowedOid<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut iter = self.0.iter();

        write!(f, "{}", iter.next().expect("no first oid value"))?;

        for oid in iter {
            write!(f, ".{}", oid)?;
        }

        Ok(())
    }
}

impl crate::oid_safety::ReadableOid for BorrowedOid<'_> {}
impl crate::oid_safety::WritableOid for BorrowedOid<'_> {}

pub(crate) struct BorrowedOctetString<'a>(pub(crate) &'a [u8]);

impl Serialize for BorrowedOctetString<'_> {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(OCTET_STRING_TAG);
        Length::new(self.0.len() as u64).serialize(buffer);
        buffer.write(self.0);
    }
}

pub(crate) struct CustomTagSeq<'a> {
    tag: u8,
    items: &'a [&'a dyn Serialize],
}

impl<'a> CustomTagSeq<'a> {
    pub(crate) fn new(tag: u8, items: &'a [&'a dyn Serialize]) -> Self {
        Self { tag, items }
    }
}

impl Serialize for CustomTagSeq<'_> {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(self.tag);
        buffer.write_byte(0x82); // long length, 2 bytes

        let seq_idx = buffer.len();
        buffer.write(&[0, 0]); // premade 16-bit length value
        let len_start = seq_idx + 2;

        for item in self.items {
            item.serialize(buffer);
        }

        let seq_len = (buffer.len() - len_start) as u16;
        buffer[seq_idx..seq_idx + 2].copy_from_slice(&seq_len.to_be_bytes()[..]);
    }
}

pub(crate) struct NullValueOids<'a, T: Iterator<Item = &'a [u64]> + Clone>(pub(crate) T);

impl<'a, T: Iterator<Item = &'a [u64]> + Clone> Serialize for NullValueOids<'a, T> {
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(SEQUENCE_TAG);
        buffer.write_byte(0x82); // long length, 2 bytes

        let seq_idx = buffer.len();
        buffer.write(&[0, 0]); // premade 16-bit length value
        let len_start = seq_idx + 2;

        for oid in self.0.clone() {
            let pair: &[&dyn Serialize] = &[&BorrowedOid(oid), &Value::Null];
            pair.serialize(buffer);
        }

        let seq_len = (buffer.len() - len_start) as u16;
        buffer[seq_idx..seq_idx + 2].copy_from_slice(&seq_len.to_be_bytes()[..]);
    }
}

impl<T> Serialize for &'_ [T]
where
    T: crate::session::WritableOidAndValue,
{
    fn serialize(&self, buffer: &mut Vec<u8>) {
        buffer.write_byte(SEQUENCE_TAG);
        buffer.write_byte(0x82); // long length, 2 bytes

        let seq_idx = buffer.len();
        buffer.write(&[0, 0]); // premade 16-bit length value
        let len_start = seq_idx + 2;

        for oav in *self {
            let pair: &[&dyn Serialize] = &[&BorrowedOid(oav.oid_bytes()), &oav.oid_value()];
            pair.serialize(buffer);
        }

        let seq_len = (buffer.len() - len_start) as u16;
        buffer[seq_idx..seq_idx + 2].copy_from_slice(&seq_len.to_be_bytes()[..]);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! test_series {
        ($($s:stmt);+$(;)?) => {{
            (|| -> Result<(), DeserializeError> { $($s)+ Ok(()) })().unwrap();
        }}
    }

    #[test]
    fn deserialize_ints() {
        test_series!(
            assert_eq!(i32::deserialize(&mut &[0x02, 0x01, 0x05][..])?, 0x05);
            assert_eq!(i32::deserialize(&mut &[0x02, 0x01, 0x05][..])?, 0x05);
            assert_eq!(u32::deserialize(&mut &[0x02, 0x04, 0xDE, 0xAD, 0xBE, 0xEF][..])?, 0xDEAD_BEEF);
        );
    }
}
